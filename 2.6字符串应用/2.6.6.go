package main

import (
	"fmt"
	"math"
)

func main() {
	var progress = 2
	var target = 8

	// 两参数格式化
	title := fmt.Sprintf("已完成任务%d个，还需要完成%d个任务", progress, target)
	fmt.Println(title)

	pi := math.Pi
	// 按数值本身的格式输出
	variant := fmt.Sprintf("%v %v %v", "月球基地", pi, true)

	fmt.Println(variant)
	// 匿名结构体声明，并赋予初值
	profile := &struct {
		Name string
		HP   int
	}{
		Name: "rat",
		HP:   150,
	}
	fmt.Printf("使用'%%+v' %+v\n", profile)
	fmt.Printf("使用'%%#v' %#v\n", profile)
	fmt.Printf("使用'%%T' %T\n", profile)
}

// 已完成任务2个，还需要完成8个任务
// 月球基地 3.141592653589793 true
// 使用'%+v' &{Name:rat HP:150}
// 使用'%#v' &struct { Name string; HP int }{Name:"rat", HP:150}
// 使用'%T' *struct { Name string; HP int }
