package main

import (
	"fmt"
)

func main() {
	// 2.6.4 修改字符串
	angel := "Heros never die"

	angleBytes := []byte(angel)

	for i := 5; i <= 10; i++ {
		angleBytes[i] = ' '
	}

	fmt.Println(string(angleBytes))
	// Go语言中的字符串和其他高级语言（Java、C#）一样，默认是不可变的（immutable）字符串不可变有很多好处，如天生线程安全，大家使用的都是只读对象，无须加锁；
	// 再者，方便内存共享，而不必使用写时复制（Copy On Write）等技术；
	// 字符串hash值也只需要制作一份。所以说，代码中实际修改的是[]byte，[]byte在Go语言中是可变的，本身就是一个切片。

	// 总结：
	// ● Go语言的字符串是不可变的。
	// ● 修改字符串时，可以将字符串转换为[]byte进行修改。
	// ● []byte和string可以通过强制类型转换互转。
}
