package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println(getValue("config.ini", "remote \"origin\"", "fetch"))
	fmt.Println(getValue("config.ini", "core", "bare"))
}

func getValue(filename, expectSection, expectKey string) string {
	// 打开文件
	file, err := os.Open(filename)
	// 文件找不到或者异常
	if err != nil {
		return "文件不存在或者打开异常"
	}

	// 使用读取器读取文件
	reader := bufio.NewReader(file)

	// 当前读取的段的名称
	var sectionName string

	for {
		// 读取文件的一行
		linestr, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		// 去除左右两边的空格,\t,\n字符
		linestr = strings.TrimSpace(linestr)
		// 忽略空行或注释
		if "" == linestr || ';' == linestr[0] {
			continue
		}

		// 读取段和键值的逻辑
		// 行首和行尾分别是 [ ] 说明是段标记起止符
		if '[' == linestr[0] && ']' == linestr[len(linestr)-1] {
			// 将段名取出
			sectionName = linestr[1 : len(linestr)-1]

			// 这个段是希望读取的
		} else if sectionName == expectSection {
			// 执行读取键值逻辑
			// 切开等号分割的键值对
			pair := strings.Split(linestr, "=")

			// 保证切开只有1个等号分割的键值情况
			if 2 == len(pair) {
				// 去掉键的多余空白字符
				key := strings.TrimSpace(pair[0])

				// 是期望的键
				if key == expectKey {
					// 返回去掉空白字符的值
					return strings.TrimSpace(pair[1])
				}
			}
		}
	}

	// 执行完毕，关闭文件
	defer file.Close()
	return ""
}
