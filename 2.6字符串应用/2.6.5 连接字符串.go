package main

import (
	"bytes"
)

// 连接字符串这么简单，还需要学吗？确实，Go语言和大多数其他语言一样，使用“+”对字符串进行连接操作，非常直观。
// 但问题来了，好的事物并非完美，简单的东西未必高效。
// 除了加号连接字符串，Go语言中也有类似于StringBuilder的机制来进行高效的字符串连接，例如：

func main() {
	// 2.6.5 连接字符串 使用bytes.Buffer缓冲实现字符串拼接
	hammer := "张三"
	sickle := "，你在哪里？"
	// 声明字节缓冲
	var stringBuilder bytes.Buffer
	// 把字符串写入缓冲
	stringBuilder.WriteString(hammer)
	stringBuilder.WriteString(sickle)
	// 将缓冲以字符串形式输出
	println(stringBuilder.String())
}
