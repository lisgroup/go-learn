package main

import "fmt"

func main() {
	seq := []string{"a", "b", "c", "d", "e"}

	// 删除指定的 index
	index := 2

	//// 查询删除位置前的元素和之后的元素
	//fmt.Println(seq[:index], seq[index+1:])
	//
	//// 将删除位置前后的元素连接起来
	//seq = append(seq[:index], seq[index+1:]...)

	fmt.Println(seq)
	fmt.Println(deleteSliceByIndex(seq, index))
}

// Go语言中切片元素的删除过程并没有提供任何的语法糖或者方法封装，无论是初学者学习，还是实际使用都是极为麻烦的
func deleteSliceByIndex(seq []string, index int) []string {
	return append(seq[:index], seq[index+1:]...)
}

// 连续容器的元素删除无论是在任何语言中，都要将删除点前后的元素移动到新的位置。随着元素的增加，这个过程将会变得极为耗时。
// 因此，当业务需要大量、频繁地从一个切片中删除元素时，
// 如果对性能要求较高，就需要反思是否需要更换其他的容器（如双链表等能快速从删除点删除元素）。
