package main

import "fmt"

//type USB interface {
//	Name() string
//	Connect()
//}
type USB interface {
	Name() string
	Connect
}

// 嵌入结构
type Connect interface {
	Connect()
}

type PhoneConnecter struct {
	name string
}

func (phone PhoneConnecter) Name() string {
	return phone.name
}

func (phone PhoneConnecter) Connect() {
	fmt.Println("connect:", phone.name)
}

//func Disconnect(usb USB) {
//	if pc, ok := usb.(PhoneConnecter); ok {
//		fmt.Println("Disconnect:", pc.name)
//		return
//	}
//	fmt.Println("Disconnect Unknown")
//}
func Disconnect(usb interface{}) {
	switch v := usb.(type) {
	case PhoneConnecter:
		fmt.Println("Disconnect:", v.name)
	default:
		fmt.Println("Disconnect Unknown")
	}
}

func main() {
	var a USB
	a = PhoneConnecter{name: "PhoneConnecter"}
	a.Connect()
	Disconnect(a)

	pc := PhoneConnecter{"phone"}
	var b Connect
	b = Connect(pc)
	b.Connect()

	// 将对象赋值给接口时，会发生拷贝，
	// 而接口内部存储的是指向这个复制品的指针，既无法修改复制品的状态，也无法获取指针
	pc.Connect()
	pc.name = "pc"
	b.Connect()
	pc.Connect()
}
