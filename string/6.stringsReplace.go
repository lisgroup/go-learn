package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "hello,world"

	replaceStr := strings.Replace(str, "world", "Go", 1) // hello,Go
	fmt.Println(replaceStr)
	replaceStr = strings.Replace(str, "l", "g", -1) // heggo,worgd
	fmt.Println(replaceStr)
}
