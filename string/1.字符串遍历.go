package main

import "fmt"

func main() {
	// 字符串遍历，同时处理中文编码问题。
	str := "lisgroup--李明明"
	// 统计字符串长度，按字节算(一个中文占三位)，len(str) = 19
	rStr := []rune(str)

	rStrLen := len(rStr)
	//fmt.Println(rStrLen) // 13
	for i := 0; i < rStrLen; i++ {
		fmt.Printf("第%v位是: %c\n", i, rStr[i])
	}
	fmt.Println(rStrLen)
}
