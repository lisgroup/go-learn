package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "hello,world,it,"

	arr := strings.Split(str, ",")

	for _, val := range arr {
		fmt.Println(val)
	}
	//hello
	//world
	//it
	//  空字符
}
