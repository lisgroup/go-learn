## 常用字符串函数使用

#### 1. strings.Contains() 字符串查找返回 bool

```go
package main

import (
    "fmt"
    "strings"
)

func main() {
    str := "hello,world"
    fmt.Println(strings.Contains(str, "hello")) // true
}

```

#### 2. strings.Count() 字符串统计

```go
package main

import (
    "fmt"
    "strings"
)

func main() {
    str := "hello,world"
    fmt.Println(strings.Count(str, "hello")) // 1
    fmt.Println(strings.Count(str, "l")) // 3
}

```

#### 3. strings.EqualFold() 字符串比较不区大小写返回bool
> 注意："==" 区分大小写
```go
package main

import (
    "fmt"
    "strings"
)

func main() {
    str := "hello,world"
    fmt.Println(strings.EqualFold(str, "Hello,World")) // true
}

 ```

#### 4. strings.Index() 返回在字符串第一次出现的位置

```go
package main

import (
    "fmt"
    "strings"
)
 
func main() {
    str := "hello,world"
    // 索引从0开始，注意为一次出现位置
    fmt.Println(strings.Index(str, "o")) // 4
}

 ```

#### 5. strings.LastIndex() 返回在字符串最后出现的位置

#### 6. strings.Replace(s, old, new, n) 字符串替换，n替换次数，-1替所有

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "hello,world"

	replaceStr := strings.Replace(str, "world", "Go", 1) // hello,Go
	fmt.Println(replaceStr)
	replaceStr = strings.Replace(str, "l", "g", -1) // heggo,worgd
	fmt.Println(replaceStr)
}

 ```

#### 7. strings.Split(s, sep) 字符串拆分为数组，sep 表示用s字符串拆分


#### 8. strings.ToUpper(s) 字符串转大写，strings.ToLower(s) 转小写

#### 9. 删除字符串两边的空格或特殊字符
##### 9.1 strings.TrimSpace 删除两边的空格

##### 9.2 strings.Trim(s, cutset) 删除两边指定的字符，cutset代表要删除的字符

##### 9.3 strings.TrimLeft(s, cutset) 删除左边指定的支符

##### 9.4 strings.TrimRight(s, cutset) 删除右边指定的支符

#### 10. 判断是否指定字符开头或结尾

##### 10.1 strings.HasPrefix(s, prefix) 是否prefix开头
##### 10.2 strings.HasSuffix(s, suffix) 是否suffix结尾