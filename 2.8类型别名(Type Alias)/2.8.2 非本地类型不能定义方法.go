package main

import (
	"fmt"
	"time"
)

// 定义 MyDuration 的别名
//type MyDuration = time.Duration // ./alias.go:10:6: cannot define new methods on non-local type time.Duratio
type MyDuration time.Duration

// 为 MyDuration 添加一个函数
func (m MyDuration) EasySet(a string) {
	fmt.Println(a)
}

func main() {

}
