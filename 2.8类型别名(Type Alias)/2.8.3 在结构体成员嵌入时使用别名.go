package main

import (
	"fmt"
	"reflect"
)

// 定义商标结构体
type Brande struct {
}

// 为商标结构体添加Show()方法
func (t Brande) Show() {
}

// 为商标Brande定义一个别名FakeBrande
type FakeBrande = Brande

// 定义车辆结构体
type Vehicle struct {
	// 嵌入两个结构体
	FakeBrande
	Brande
}

func main() {
	// 声明 a 为车辆类型
	var a Vehicle

	// 指定调用 FakeBrande 的 Show() 方法
	a.FakeBrande.Show()
	//a.Show() // go:32:3: ambiguous selector a.Show

	// 取 a 的类型反射对象
	ta := reflect.TypeOf(a)

	// 遍历 a 的所有成员
	for i := 0; i < ta.NumField(); i++ {
		// a 的成员信息
		f := ta.Field(i)

		// 打印成员的字段名和类型
		fmt.Printf("Field Name: %v, Field Type: %v Type: %T \n", f.Name, f.Type.Name(), f.Name)
	}

}
