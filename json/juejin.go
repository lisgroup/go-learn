package main

import (
	"encoding/json"
	"fmt"
)

type ResultInfo struct {
	ErrNo  int    `json:"err_no"`
	ErrMsg string `json:"err_msg"`
	Data   interface{}
}

func main() {
	str := `{"err_no":0,"err_msg":"success","data":[{"article_id":"6877842011136163854","article_info":{"article_id":"6877842011136163854","user_id":"166781497383294","category_id":"6809637769959178254","tag_ids":[6809640445233070000,6809641037787562000],"visible_level":0,"link_url":"","cover_image":"","is_gfw":0,"title":"摊牌了！我要手写一个“Spring Boot”","brief_content":"今天是晴天，我像往常一样坐在窗台，看着电脑“发呆”。毕竟是周末，就不分享技术干货了。 简单和小伙伴们聊聊自己最近正在做，已经开始做的事情。 我们经常会对自己早期写的代码感觉恶心，这是导致很多项目烂尾、不好维护很重要的一个原因之一。 写作也是一样，我也经常觉得自己早期写的文章像一…","is_english":0,"is_original":1,"user_index":12.579538444576684,"original_type":0,"original_author":"","content":"","ctime":"1601372402","mtime":"1601435954","rtime":"1601435954","draft_id":"6877841789651255310","view_count":3031,"collect_count":0,"digg_count":39,"comment_count":12,"hot_index":202,"is_hot":0,"rank_index":4.73616501,"status":2,"verify_status":1,"audit_status":2,"mark_content":""},"author_user_info":{"user_id":"166781497383294","user_name":"JavaGuide","company":"公众号&Github：JavaGuide","job_title":"DEV","avatar_large":"https://user-gold-cdn.xitu.io/2020/7/15/17350490c000d6e8?w=795&h=795&f=jpeg&s=75115","level":6,"description":"Java","followee_count":129,"follower_count":34847,"post_article_count":202,"digg_article_count":637,"got_digg_count":20230,"got_view_count":1172027,"post_shortmsg_count":21,"digg_shortmsg_count":281,"isfollowed":false,"favorable_author":1,"power":33086},"category":{"category_id":"6809637769959178254","category_name":"后端","category_url":"backend","rank":1,"ctime":1457483880,"mtime":1432503193,"show_type":3},"tags":[{"id":2546553,"tag_id":"6809640445233070094","tag_name":"Java","color":"#DD2C2A","icon":"https://lc-gold-cdn.xitu.io/f8ee3cd45f949a546263.png","back_ground":"","show_navi":0,"tag_alias":"","post_article_count":50864,"concern_user_count":258661},{"id":2546982,"tag_id":"6809641037787561992","tag_name":"Spring Boot","color":"#000000","icon":"https://lc-gold-cdn.xitu.io/f77e4a02edb8b963a2c5.png","back_ground":"","show_navi":0,"tag_alias":"","post_article_count":5336,"concern_user_count":27396}],"user_interact":{"id":6877842011136164000,"omitempty":2,"user_id":3667626521790151,"is_digg":false,"is_follow":false,"is_collect":false}}],"cursor":"eyJ2IjoiNjg3Nzg0MjAxMTEzNjE2Mzg1NCIsImkiOjIwfQ==","count":31740,"has_more":true}`
	res, err := Json_decode(str)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}
}

func Json_decode(data string) (map[string]interface{}, error) {
	var dat map[string]interface{}
	err := json.Unmarshal([]byte(data), &dat)
	return dat, err
}

func Json_encode(data interface{}) (string, error) {
	jsons, err := json.Marshal(data)
	return string(jsons), err
}
