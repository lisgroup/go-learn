package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Result struct {
	ROI   string `json:"roi"`
	Score string `json:"score"`
}

type StudentInfo struct {
	Id      int         `json:"id"`
	Name    string      `json:"name"`
	Results interface{} `json:"results"`
}

func main() {
	data, _ := ioutil.ReadFile("studentInfo.json")
	fmt.Println(string(data))
	da := `{
  "id": 2019940606,
  "name": "shenziheng",
  "results": {
    "mathmatic school": {
      "roi": "mathematic",
      "score": "A"
    },
    "conputer school": {
      "roi": "computer",
      "score": "A+"
    }
  }
}`
	data = []byte(da)

	var student StudentInfo
	_ = json.Unmarshal(data, &student)
	fmt.Printf("Students infomation is %v \n", student)

	fmt.Printf("Students result is %T , %v \n", student.Results, student.Results)
	res := student.Results
	for index, value := range res.(map[string]interface{}) {
		fmt.Printf("Students infomation is Index=%v, value=%v\n", index, value)
	}
}
