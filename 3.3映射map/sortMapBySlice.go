package main

import (
	"fmt"
	"sort"
)

func main() {
	myMap := map[int]string{1: "a", 2: "b", 3: "c", 4: "d", 5: "e"}

	fmt.Println(myMap)
	mySlice := make([]int, len(myMap))

	// 计数器
	i := 0
	for k := range myMap {
		mySlice[i] = k
		i++
	}

	fmt.Println(mySlice)
	sort.Ints(mySlice)
	fmt.Println(mySlice)

	for _, v := range mySlice {
		//fmt.Println(v)
		fmt.Println(myMap[v])
	}
}
