package main

import (
	"fmt"
)

func main() {
	myMap := map[int]string{1: "a", 2: "b", 3: "c", 4: "d", 5: "e"}
	fmt.Println(myMap)

	map2 := make(map[string]int, len(myMap))

	for k, v := range myMap {
		map2[v] = k
	}

	fmt.Println(map2)
}
