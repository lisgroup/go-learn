package main

import (
	"flag"
	"fmt"
	"unicode/utf8"
)

func main() {
	// 定义命令行参数
	mode := flag.String("mode", "default", "process mode")
	// 解析命令行参数
	flag.Parse()
	// 输出命令行参数
	fmt.Println(*mode)

	str := new(string)
	*str = "中国"
	fmt.Println(*str)
	// 检测字符串UTF-8格式的长度
	fmt.Println(utf8.RuneCountInString(*str + "test"))
}

// go run flagparse.go --mode=fast

// 2.4.5 创建指针的另一种方法—new()函数
func returnString() {
	str := new(string)
	*str = "张三"
	fmt.Println(*str) // 输出： 张三
}
