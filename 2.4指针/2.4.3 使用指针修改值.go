package main

import (
	"fmt"
	"math"
)

func main() {
	var num int16 = 1000
	name := "test"

	fmt.Println("int8 range:", math.MinInt8, math.MaxInt8)
	fmt.Println("int16 range:", math.MinInt16, math.MaxInt16)
	fmt.Println("int32 range:", math.MinInt32, math.MaxInt32)
	fmt.Println("int64 range:", math.MinInt64, math.MaxInt64)

	// 输出十六进制和十进制
	fmt.Printf("num 16进制: 0x%x %d\n", num, num)
	var a int32 = 1047483647
	fmt.Printf("int32: 0x%x %d\n", a, a)

	fmt.Printf("num address: %p, name : %p\n", &num, &name)
	// 打印类型
	ptr := &a
	fmt.Printf("ptr type: %T\n", ptr)
	fmt.Printf("value: %d\n", *ptr)

	c, d := 1, 2
	fmt.Printf("c : %p, d : %p\n", &c, &d)
	swap(&c, &d)
	fmt.Printf("c : %p, d : %p\n", &c, &d)
	fmt.Println(c, d)
}

func swap(c, d *int) {
	fmt.Println(*c, *d)
	fmt.Printf("a : %p, b : %p\n", c, d)
	//*a, *b = *b, *a
	c, d = d, c
	fmt.Printf("a : %p, b : %p\n", c, d)
	fmt.Println(*c, *d)
}

// 结果表明，交换是不成功的。
// 上面代码中的swap()函数交换的是a和b的地址，在交换完毕后，a和b的变量值确实被交换。但和a、b关联的两个变量并没有实际关联。
// 这就像写有两座房子的卡片放在桌上一字摊开，交换两座房子的卡片后并不会对两座房子有任何影响。
// 个人理解： 函数内部的作用域不影响外部变量
