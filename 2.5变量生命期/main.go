package main

import "fmt"

// 本函数测试入口参数和返回值情况
func dummy(b int) int {
	// 声明c赋值进入参数并返回
	var c int
	c = b

	return c
}

// 空函数
func void() {

}

func main() {
	// 声明 a 并打印
	var a int
	// 调用void
	void()
	// 打印a变量和 dummy()函数返回
	fmt.Println(a, dummy(0))
}

//go run -gcflags "-m -l" main.go
// 程序运行结果分析如下：
//● 输出第2行告知“main的第29行的变量a逃逸到堆”。
//● 第3行告知“dummy(0)调用逃逸到堆”。由于dummy()函数会返回一个整型值，这个值被fmt.Println使用后还是会在其声明后继续在main()函数中存在。
//● 第4行，这句提示是默认的，可以忽略。

// 上面例子中变量c是整型，其值通过dummy()的返回值“逃出”了dummy()函数。c变量值被复制并作为dummy()函数返回值返回，
// 即使c变量在dummy()函数中分配的内存被释放，也不会影响main()中使用dummy()返回的值。
// c变量使用栈分配不会影响结果。
