package main

import (
	"fmt"
	"reflect"
)

type UserTest struct {
	Id   int
	Name string
	Age  int
}

func (u UserTest) Hello(name string) {
	fmt.Println("Hello world.", name)
}

func main() {
	u := UserTest{1, "li", 20}
	t := reflect.TypeOf(u)
	// 判断类型是否合法
	if k := t.Kind(); k != reflect.Struct {
		fmt.Println("type error")
		return
	}
	// 获取函数名称
	val := reflect.ValueOf(u)
	for i := 0; i < t.NumMethod(); i++ {
		method := t.Method(i)
		fmt.Printf("%6s : %v\n", method.Name, method.Type)
		// 函数名称
		funcCall := val.MethodByName(method.Name)
		// 函数参数
		args := []reflect.Value{reflect.ValueOf("jack")}
		funcCall.Call(args)
	}

}

func (u UserTest) Action(name string) {
	fmt.Println("Hello", name, ", my name is ", u.Name)
}
