package main

import (
	"fmt"
	"reflect"
)

type OldUser struct {
	Id   int
	Name string
	Age  int
}

// 使用反射调用结构体的方法
func main() {
	u := OldUser{11, "li", 20}
	v := reflect.ValueOf(u)
	funcCall := v.MethodByName("World")

	// 函数参数 slice
	args := []reflect.Value{reflect.ValueOf("jack")}
	funcCall.Call(args)
}

// 结构体方法
func (u OldUser) World(name string) {
	fmt.Println("Hello", name, ", my name is ", u.Name)
}
