package main

import (
	"fmt"
	"reflect"
)

type User struct {
	Id   int
	Name string
	Age  int
}

func (u User) Hello() {
	fmt.Println("Hello world.")
}

func main() {
	u := User{1, "li", 20}
	Info(u)
}

func Info(o interface{}) {
	// 类型名称
	t := reflect.TypeOf(o)
	// 判断类型是否合法
	if k := t.Kind(); k != reflect.Struct {
		fmt.Println("type error!")
		return
	}
	fmt.Println("Type: ", t.Name())

	// 属性信息
	v := reflect.ValueOf(o)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		val := v.Field(i).Interface()
		fmt.Printf("%6s; %v = %v\n", f.Name, f.Type, val)
	}
	// 方法信息
	for i := 0; i < t.NumMethod(); i++ {
		m := t.Method(i)
		fmt.Printf("%6s: %v\n", m.Name, m.Type)
	}
}
