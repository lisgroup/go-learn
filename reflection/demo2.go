package main

import (
	"fmt"
	"reflect"
)

type MyUser struct {
	Id   int
	Name string
	Age  int
}

type Manage struct {
	MyUser
	title string
}

func (u MyUser) Hello() {
	fmt.Println("Hello world.")
}

func main() {
	//m := Manage{MyUser: MyUser{Id: 1, Name: "ll", Age: 29}, title: "title"}
	m := Manage{MyUser{Id: 1, Name: "ll", Age: 29}, "title"}
	t := reflect.TypeOf(m)
	// 取出匿名字段
	fmt.Printf("%#v\n", t.Field(0))
	fmt.Printf("%#v\n", t.Field(1))

	// MyUser.Id {0, 0}
	fmt.Printf("%#v\n", t.FieldByIndex([]int{0, 0}))
	// MyUser.Name {0, 1}
	fmt.Printf("%#v\n", t.FieldByIndex([]int{0, 1}))
}
