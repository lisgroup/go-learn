package main

import (
	"fmt"
	"reflect"
)

type NewUser struct {
	Id   int
	Name string
	Age  int
}

func main() {
	x := 123
	// 传递指针
	v := reflect.ValueOf(&x)
	// Elem() 方法设置指针的值
	v.Elem().SetInt(999)
	fmt.Println(x)

	// 修改结构体的字段值
	u := NewUser{11, "zzz", 18}
	fmt.Println("init Name:", u.Name)
	Set(&u)
	fmt.Println("Change Name:", u.Name)
}

// 通过指针传递修改值
func Set(p interface{}) {
	v := reflect.ValueOf(p)
	if v.Kind() != reflect.Ptr || !v.Elem().CanSet() {
		fmt.Println("类型错误或者不允许修改值")
		return
	}
	// 查询值
	f := v.Elem().FieldByName("Name")
	if !f.IsValid() {
		fmt.Println("Fail")
		return
	}
	if f.Kind() == reflect.String {
		f.SetString("NoNoNo")
	}
}
