package main

import (
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func downloadImage(path, imageUrl string) {
	savePath := path + "/" + MakeYearDaysRand(11) + ".jpg"
	fil, err := os.Create(savePath)
	if err != nil {
		log.Fatal("file Created err:", err)

	}
	defer fil.Close()
	// 下载图片
	response, err := http.Get(imageUrl)
	if err != nil {
		log.Fatal("httpGet err:", err)
	}
	defer response.Body.Close()
	buf := make([]byte, 4096)
	for {
		n, err2 := response.Body.Read(buf)
		if n == 0 {
			break
		}
		if err2 != nil && err2 != io.EOF {
			log.Fatal("read body err:", err2)
		}
		fil.Write(buf[:n])
	}
}

//生成单号
//06123xxxxx
//sum 最少10位,sum 表示全部单号位数
func MakeYearDaysRand(sum int) string {
	//年
	strs := time.Now().Format("06")
	//一年中的第几天
	days := strconv.Itoa(GetDaysInYearByThisYear())
	count := len(days)
	if count < 3 {
		//重复字符0
		days = strings.Repeat("0", 3-count) + days
	}
	//组合
	strs += days
	//剩余随机数
	sum = sum - 5
	if sum < 1 {
		sum = 5
	}
	//0~9999999的随机数
	//ran := GetRand()
	rand.Seed(time.Now().Unix())
	pow := math.Pow(10, float64(sum)) - 1
	//fmt.Println("sum=>", sum)
	//fmt.Println("pow=>", pow)
	result := strconv.Itoa(rand.Intn(int(pow)))
	count = len(result)
	//fmt.Println("result=>", result)
	if count < sum {
		//重复字符0
		result = strings.Repeat("0", sum-count) + result
	}
	//组合
	strs += result
	return strs
}

//年中的第几天
func GetDaysInYearByThisYear() int {
	now := time.Now()
	total := 0
	arr := []int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	y, month, d := now.Date()
	m := int(month)
	for i := 0; i < m-1; i++ {
		total = total + arr[i]
	}
	if (y%400 == 0 || (y%4 == 0 && y%100 != 0)) && m > 2 {
		total = total + d + 1

	} else {
		total = total + d
	}
	return total
}

func main() {
	path := "./"
	imageUrl := "https://img9.doubanio.com/view/photo/s_ratio_poster/public/p480383375.jpg"
	downloadImage(path, imageUrl)
}
