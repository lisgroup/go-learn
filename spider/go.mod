module spider

go 1.15

require (
	github.com/antchfx/htmlquery v1.2.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lisgroup/easyhttp v1.1.1
	golang.org/x/net v0.0.0-20200927032502-5d4f70055728
)
